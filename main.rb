print "Input Path: "
path = gets.strip
dir_path = "DropsuiteTest/#{path}"

if Dir.exist?(dir_path)
	counts = Hash.new(0)
	Dir.glob("#{dir_path}**/*").each do |entry|
		unless File.directory?(entry)
			file = File.open(entry, "rb")
			contents = file.read			
			counts[contents] = counts[contents]+1 
			counts.keys.each {|c| counts[c] = counts[c]+1 if contents.include?(c) && !contents.eql?(c)}
			file.close
		end
	end
	puts counts
	counts.select {|a, b| puts "biggest value is #{a} with count is #{b}" if counts[a] == counts.values.max}
else
	puts "Path doesn't exist"
end